from classes import Corpus, FeatureVector, Train, Test


def main():
    do_train = True
    do_test = True
    do_comp = True
    train_set_file = "train.labeled"
    test_set_file = "test.labeled"
    comp_set_file = "comp.unlabeled"
    perceptron_iterations = 20

    train_set = Corpus(train_set_file, insert_labels=True)
    test_set = Corpus(test_set_file, insert_labels=False)
    comp_set = Corpus(comp_set_file, insert_labels=False)

    feature_vector = FeatureVector(train_set)
    train = Train(train_set, feature_vector)
    train.generate_features()

    if do_train:
        train.evaluate_feature_vectors()
        train.perceptron(perceptron_iterations)

    if do_test:
        model_name = "{}_iterations".format(perceptron_iterations)
        test = Test(test_set, feature_vector)
        test.load_model(model_name)

        test.evaluate_feature_vectors()
        test.inference()

        labeled_test_set = Corpus(test_set_file, insert_labels=True)
        accuracy = test.evaluate_model(labeled_test_set)
        print(accuracy)
        test.print_results_to_file(labeled_test_set, model_name, is_test=True)
        test.print_accuracy(model_name, str(accuracy))

    if do_comp:
        pass  # TODO


if __name__ == "__main__":
    main()
